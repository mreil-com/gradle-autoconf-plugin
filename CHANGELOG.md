# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [0.0.4] - 2018-10-12
### Changed
- Use SpotBugs instead of FindBugs


## [0.0.3] - 2018-10-09
### Added
- Support JUnit 4


## [0.0.2] - 2018-10-07
### Changed
- Fix wrapper task configuration

### Added
- Enforce Gradle version to be at least 4.6


## [0.0.1] - 2018-10-01
### Added
- Default repositories
- Default tasks
- Default config for Java projects
- Default config for Groovy projects
- Release plugin configuration


[0.0.4]: ../../branches/compare/v0.0.4..v0.0.3#diff
[0.0.3]: ../../branches/compare/v0.0.3..v0.0.2#diff
[0.0.2]: ../../branches/compare/v0.0.2..v0.0.1#diff
[0.0.1]: ../../src/v0.0.1
[Unreleased]: ../../src
