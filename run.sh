#!/usr/bin/env bash

./gradlew --console=plain clean check build &
PID=$!

while(true); do
    free
    ps $PID > /dev/null || break
    sleep 1
done

wait
