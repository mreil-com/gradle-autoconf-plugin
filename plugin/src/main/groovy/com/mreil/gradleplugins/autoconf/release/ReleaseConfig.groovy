package com.mreil.gradleplugins.autoconf.release


import org.gradle.api.GradleException
import org.gradle.api.Project

import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasReleasePlugin

class ReleaseConfig {
    def apply(Project project) {
        if (hasReleasePlugin(project)) {
            if (project != project.rootProject) {
                throw new GradleException("release plugin should only be configured on root project")
            }

            configureReleasePlugin(project)
        }
    }

    static def configureReleasePlugin(Project project) {
        project.with {
            release {
                tagTemplate = 'v${version}'
                git {
                    requireBranch = /master|support\/.*/
                    pushToRemote = 'origin'
                    pushToBranchPrefix = ''
                    commitVersionFileOnly = false
                    signTag = false
                }
            }
        }
    }
}
