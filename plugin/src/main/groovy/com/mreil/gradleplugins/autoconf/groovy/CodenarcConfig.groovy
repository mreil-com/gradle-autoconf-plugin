package com.mreil.gradleplugins.autoconf.groovy

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import com.mreil.gradleplugins.autoconf.ConfigDownloader
import org.gradle.api.Project
import org.gradle.api.plugins.quality.CodeNarc

/**
 * Configure codenarc.
 * Executed in the afterEvaluate phase.
 */
class CodenarcConfig {
    static final def DEFAULT_CODENARC_CONFIG =
            "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/codenarc/codenarc.xml"

    def apply(Project project) {
        configurePlugin(project)
        configureDownloadTask(project)
    }

    static def configurePlugin(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        project.with {
            apply plugin: "codenarc"

            codenarc {
                toolVersion = extension.codenarcVersion
            }

            tasks.withType(CodeNarc) {
                reports {
                    xml.enabled false
                    html.enabled true
                }
            }
        }
    }

    static configureDownloadTask(Project project) {
        def ask = project.task("downloadCodenarcConfig").doLast {
            File configFile = project.codenarc.configFile
            def dl = new ConfigDownloader(DEFAULT_CODENARC_CONFIG,
                    configFile,
                    project.logger)
            dl.download()
        }

        project.tasks.getByName("codenarcMain").dependsOn ask
        project.tasks.getByName("codenarcTest").dependsOn ask
    }
}
