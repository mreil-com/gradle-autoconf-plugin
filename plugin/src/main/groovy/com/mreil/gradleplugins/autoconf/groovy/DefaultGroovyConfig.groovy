package com.mreil.gradleplugins.autoconf.groovy

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import org.gradle.api.Project

import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasGradlePluginPlugin
import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasGroovyPlugin

class DefaultGroovyConfig {

    static apply(Project project) {
        project.allprojects.each { sub ->
            configureDefaultGroovyConfig(sub)
        }
    }

    static def configureDefaultGroovyConfig(Project project) {
        project.afterEvaluate {
            if (hasGroovyPlugin(project)) {
                configureGroovyConfig(project)
            }
        }
    }

    static def configureGroovyConfig(Project project) {
        configureDefaultDependencies(project)
        configureTestFramework(project)
        configureCodenarcConfig(project)
        configureGroovyPlugin(project)
    }

    static def configureGroovyPlugin(Project project) {
        new GradlePluginConfig().apply(project)
    }

    static def configureCodenarcConfig(Project project) {
        new CodenarcConfig().apply(project)
    }

    static def configureDefaultDependencies(Project project) {
        if (!hasGradlePluginPlugin(project)) {

            def extension = project.rootProject.extensions
                    .getByName(AutoconfExtension.NAME) as AutoconfExtension

            project.dependencies {
                compile "org.codehaus.groovy:groovy:${extension.defaultVersions.groovy}"
            }
        }
    }

    static def configureTestFramework(Project project) {
        configureSpock(project)
    }

    static def configureSpock(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        project.with {
            dependencies {
                testCompile "org.spockframework:spock-core:${extension.defaultVersions.spock}"
                testRuntime "org.objenesis:objenesis:${extension.defaultVersions.objenesis}"
            }
        }
    }
}
