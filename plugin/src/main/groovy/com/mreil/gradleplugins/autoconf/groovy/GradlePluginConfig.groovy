package com.mreil.gradleplugins.autoconf.groovy


import org.gradle.api.Project

import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasGradlePluginPlugin

class GradlePluginConfig {
    def apply(Project project) {
        if (hasGradlePluginPlugin(project)) {
            configurePlugin(project)
        }
    }

    static def configurePlugin(Project project) {
        project.with {
            // remove logback classic, SLF4J bindings are included in gradleTestKit
            configurations.findAll { conf ->
                conf.exclude module: "logback-classic"
            }

            dependencies {
                testCompile gradleTestKit()
            }
        }
    }
}
