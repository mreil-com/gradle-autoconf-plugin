package com.mreil.gradleplugins.autoconf.java

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication

class MavenConfig {
    def apply(Project project) {
        project.plugins.apply("maven-publish")

        project.with {
            publishing {
                publications {
                    maven(MavenPublication) {
                        groupId = project.rootProject.group
                        if (!groupId) { groupId = project.group }
                        if (!groupId || groupId == project.rootProject.name) {
                            throw new GradleException("The maven groupId doesn't seem to be set. Please set" +
                                    " 'project.group' on the root project.")
                        }

                        artifactId = project.name

                        from components.java
                    }
                }
                repositories {
                    mavenLocal()
                }
            }
        }
    }
}
