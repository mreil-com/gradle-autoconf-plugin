package com.mreil.gradleplugins.autoconf.java

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import org.gradle.api.Project

/**
 * Configure jacoco.
 * Executed in the afterEvaluate phase.
 */
class JacocoConfig {

    def apply(Project project) {
        configurePlugin(project)
    }

    static def configurePlugin(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        project.with {
            apply plugin: "jacoco"

            jacoco {
                toolVersion = extension.jacocoVersion
            }

            jacocoTestReport {
                reports {
                    xml.enabled false
                    csv.enabled false
                    html.destination file("${project.buildDir}/reports/jacocoHtml")
                }
                sourceDirectories = files(
                        "${project.projectDir}/src/main/java",
                        "${project.projectDir}/src/main/groovy")
                classDirectories =  files(
                        "${project.buildDir}/classes/java/main",
                        "${project.buildDir}/classes/groovy/main")
            }

            check.dependsOn jacocoTestReport
        }
    }
}
