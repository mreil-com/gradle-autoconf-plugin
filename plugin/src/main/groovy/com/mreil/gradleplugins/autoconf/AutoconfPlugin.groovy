package com.mreil.gradleplugins.autoconf

import com.mreil.gradleplugins.autoconf.groovy.DefaultGroovyConfig
import com.mreil.gradleplugins.autoconf.java.DefaultJavaConfig
import com.mreil.gradleplugins.autoconf.release.ReleaseConfig
import com.mreil.gradleplugins.autoconf.repositories.DefaultRepositories
import com.mreil.gradleplugins.autoconf.tasks.DefaultTasks
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.util.VersionNumber

import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasGradlePluginPlugin
import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasGroovyPlugin

class AutoconfPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        def gradleVersion = VersionNumber.parse(project.gradle.gradleVersion)
        if (gradleVersion.compareTo(VersionNumber.parse("4.6")) < 0) {
            throw new GradleException("Plugin needs at least Gradle version 4.6")
        }

        project.extensions.create(AutoconfExtension.NAME,
                AutoconfExtension,
                project)

        checkRootProject(project)
        checkPluginWithoutGroovy(project)
        applyBasePlugin(project)

        new DefaultRepositories().apply(project)
        new DefaultJavaConfig().apply(project)
        new DefaultGroovyConfig().apply(project)
        new ReleaseConfig().apply(project)

        // This must be last as tasks may be defined by other plugins.
        new DefaultTasks().apply(project)

        project.tasks.create("autoconf", AutoconfTask)
        project.tasks.create(DefaultVersionsTask.NAME, DefaultVersionsTask)
    }

    static def checkPluginWithoutGroovy(Project project) {
        if (hasGradlePluginPlugin(project) && !hasGroovyPlugin(project)) {
            project.logger.warn("Detected plugin plugin without groovy plugin. It is very likely you want " +
                    "the groovy plugin, too.")
        }
    }

    static def applyBasePlugin(Project project) {
        project.plugins.apply("base")
    }

    static def checkRootProject(Project project) {
        if (project.parent != null) {
            throw new GradleException("Please configure this plugin in the root project.")
        }
    }
}
