package com.mreil.gradleplugins.autoconf.tasks

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import org.gradle.api.Project
import org.gradle.api.tasks.wrapper.Wrapper

class DefaultTasks {
    static def apply(Project project) {
        createDefaultTasks(project)
        createWrapperTask(project)
    }

    static Object createWrapperTask(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        project.afterEvaluate {
            project.with {
                wrapper {
                    gradleVersion = extension.gradleVersion
                    distributionType Wrapper.DistributionType.ALL
                }
            }
        }
    }

    private static def createDefaultTasks(Project project) {
        project.allprojects*.each {
            createTasksForProject(it)
        }
    }

    private static def createTasksForProject(Project project) {
        project.logger.info("Creating default tasks for project: " + project)
        project.afterEvaluate { Project it ->
            // "test" is normally defined by the java plugin, make it available for all projects
            createTaskIfNotExists(it, "test", "Verification")

            // "publish" is already used by the maven-publishing plugin, make it the default publishing task
            createTaskIfNotExists(it, "publish", "Publishing")
        }
    }

    static def createTaskIfNotExists(Project project, String task, String groupName = null) {
        if (!project.tasks.findByName(task)) {
            project.task task, {
                group = groupName
            }
        }
    }
}
