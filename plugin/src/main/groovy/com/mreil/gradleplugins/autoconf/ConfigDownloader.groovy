package com.mreil.gradleplugins.autoconf

import org.gradle.api.logging.LogLevel
import org.slf4j.Logger

class ConfigDownloader {
    private final String url
    private final File target
    private final Logger logger

    ConfigDownloader(String url,
                     File target,
                     Logger logger) {
        this.url = url
        this.target = target
        this.logger = logger
    }

    def download() {
        if (!target.exists()) {
            logger.log(LogLevel.WARN,
                    "Local file not found. \nDownloading from: ${url}")

            target.parentFile.mkdirs()
            target.withOutputStream { out ->
                out << new URL(url).openStream()
            }
        }

    }
}
