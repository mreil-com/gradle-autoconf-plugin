package com.mreil.gradleplugins.autoconf.java

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import org.gradle.api.Project

import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasGroovyPlugin
import static com.mreil.gradleplugins.autoconf.ProjectHelper.hasJavaPlugin

class DefaultJavaConfig {

    static apply(Project project) {
        project.allprojects.each { sub ->
            configureDefaultJavaConfig(sub)
        }
    }

    static def configureDefaultJavaConfig(Project project) {
        project.afterEvaluate {
            if (hasJavaPlugin(project)) {
                configureJavaConfig(project)
            }
        }
    }

    static def configureJavaConfig(Project project) {
        configureTestReport(project)
        configureDependencyManagement(project)
        configureDefaultDependencies(project)
        configureTestFramework(project)
        configureCheckstyleConfig(project)
        configureSpotbugsConfig(project)
        configurePmd(project)
        configureJacoco(project)
        configureMavenPublish(project)
    }

    static def configureMavenPublish(Project project) {
        new MavenConfig().apply(project)
    }

    static def configureJacoco(Project project) {
        new JacocoConfig().apply(project)
    }

    static def configureSpotbugsConfig(Project project) {
        new SpotbugsConfig().apply(project)
    }

    static def configurePmd(Project project) {
        new PmdConfig().apply(project)
    }

    static def configureCheckstyleConfig(Project project) {
        new CheckstyleConfig().apply(project)
    }

    static def configureTestFramework(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        if (hasGroovyPlugin(project)) {
            return
        }
        if (extension.junit5) {
            configureJunit5(project)
        } else {
            configureJunit4(project, extension)
        }
    }

    static def configureJunit4(Project project, AutoconfExtension extension) {
        project.with {
            dependencies {
                testCompile "junit:junit:${extension.defaultVersions.junit4}"
            }
        }
    }

    static def configureJunit5(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension
        def defaultVersions = extension.defaultVersions

        project.with {
            dependencies {
                testRuntime "org.junit.platform:junit-platform-launcher:${defaultVersions.junit5Platform}"
                testCompile "org.junit.jupiter:junit-jupiter-api:${defaultVersions.junit5}"
                testRuntime "org.junit.jupiter:junit-jupiter-engine:${defaultVersions.junit5}"
            }
            test {
                useJUnitPlatform()
            }

            def junit = "junit"
            configurations.findAll {
                it.dependencies*.group.contains("org.junit.jupiter") &&
                        it.dependencies*.group.contains(junit)
            }.forEach {
                it.exclude group: it
            }
        }
    }

    static def configureTestReport(Project project) {
        project.with {
            test {
                reports {
                    junitXml.enabled = true
                    html.enabled = true
                }
            }
        }
    }

    static def configureDefaultDependencies(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension
        def defaultVersions = extension.defaultVersions

        project.with {
            dependencies {
                compile "org.slf4j:slf4j-api:${defaultVersions.slf4j}"
                testCompile "ch.qos.logback:logback-classic:${defaultVersions.logback}"
            }
        }
    }

    static def configureDependencyManagement(Project project) {
        new DefaultDependencyManagement().apply(project)
    }
}
