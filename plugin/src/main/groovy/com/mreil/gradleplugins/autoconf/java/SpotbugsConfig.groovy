package com.mreil.gradleplugins.autoconf.java

import com.github.spotbugs.SpotBugsTask
import com.mreil.gradleplugins.autoconf.AutoconfExtension
import com.mreil.gradleplugins.autoconf.ConfigDownloader
import org.gradle.api.Project

/**
 * Configure spotbugs.
 * Executed in the afterEvaluate phase.
 */
class SpotbugsConfig {
    static final def DEFAULT_SPOTBUGS_CONFIG =
            "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/spotbugs/excludeFilter.xml"

    def apply(Project project) {
        configurePlugin(project)
        configureDownloadTask(project)
    }

    static def configurePlugin(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        // backwards compatibility with findbugs
        def config = project.file("${project.rootDir}/config/findbugs/excludeFilter.xml")
        if (config.exists()) {
            project.logger.warn("Re-using FindBugs config file ${config}. For SpotBugs, please move " +
                    "the file to config/spotbugs.")
        } else {
            config = project.file("${project.rootDir}/config/spotbugs/excludeFilter.xml")
        }

        project.with {
            apply plugin: "com.github.spotbugs"
                spotbugs {
                toolVersion = extension.spotbugsVersion
                    excludeFilter = config
            }

            tasks.withType(SpotBugsTask) {
                reports {
                    xml.setEnabled false
                    html.setEnabled true
                }
            }
        }
    }

    static configureDownloadTask(Project project) {
        def ask = project.task("downloadSpotbugsConfig").doLast {
            File configFile = project.spotbugs.excludeFilter
            def dl = new ConfigDownloader(DEFAULT_SPOTBUGS_CONFIG,
                    configFile,
                    project.logger)
            dl.download()
        }

        project.tasks.getByName("spotbugsMain").dependsOn ask
        project.tasks.getByName("spotbugsTest").dependsOn ask
    }

}
