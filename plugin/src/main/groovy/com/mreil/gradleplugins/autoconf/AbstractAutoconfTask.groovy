package com.mreil.gradleplugins.autoconf

import org.gradle.api.DefaultTask

class AbstractAutoconfTask extends DefaultTask {
    AbstractAutoconfTask() {
        group = "Autoconf"
    }
}
