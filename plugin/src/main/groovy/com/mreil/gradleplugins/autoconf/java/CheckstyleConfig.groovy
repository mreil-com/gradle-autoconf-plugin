package com.mreil.gradleplugins.autoconf.java

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import com.mreil.gradleplugins.autoconf.ConfigDownloader
import org.gradle.api.Project
import org.gradle.api.plugins.quality.Checkstyle

/**
 * Configure checkstyle.
 * Executed in the afterEvaluate phase.
 */
class CheckstyleConfig {
    static final def DEFAULT_CHECKSTYLE_CONFIG =
            "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/checkstyle/checkstyle.xml"

    def apply(Project project) {
        configurePlugin(project)
        configureDownloadTask(project)
    }

    def configurePlugin(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        project.with {
            apply plugin: "checkstyle"

            dependencies {
                checkstyle "com.puppycrawl.tools:checkstyle:${extension.checkstyleVersion}"
            }

            checkstyle {
                configDir project.file("${project.rootDir}/config/checkstyle")
            }

            tasks.withType(Checkstyle) {
                reports {
                    xml.enabled false
                    html.enabled true
                }
            }
        }
    }

    static configureDownloadTask(Project project) {
        def ask = project.task("downloadCheckstyleConfig").doLast {
            File configFile = project.checkstyle.configFile
            def dl = new ConfigDownloader(DEFAULT_CHECKSTYLE_CONFIG,
                    configFile,
                    project.logger)
            dl.download()
        }

        project.tasks.getByName("checkstyleMain").dependsOn ask
        project.tasks.getByName("checkstyleTest").dependsOn ask
    }
}
