package com.mreil.gradleplugins.autoconf

import org.gradle.api.Project

class AutoconfExtension {
    public static final String NAME = "autoconf"

    Map<String, String> defaultVersions = [
            cglib              : "3.2.8",
            commonsCodec       : "1.11",
            commonsIo          : "2.6",
            commonsLang3       : "3.8.1",
            commonsCollections4: "4.2",
            feign              : " 9.6.0 ",
            groovy             : "2.4.15",
            guava              : "23.0",
            h2                 : "1.4.197",
            jackson            : "2.8.11",
            jgit               : "4.9.0.201710071750-r",
            junit4             : "4.12",
            junit5             : "5.2.0",
            junit5Platform     : "1.2.0",
            logback            : "1.2.3",
            objenesis          : "2.5.1",
            slf4j              : "1.7.25",
            spock              : "1.2-groovy-2.4",
            wiremock           : "2.19.0",
    ]

    Project project

    /**
     * Used for wrapper task
     */
    String gradleVersion = "4.9"

    boolean junit5 = true

    String checkstyleVersion = "8.13"
    String spotbugsVersion = "4.0.0"
    String codenarcVersion = "1.2.1"
    String pmdVersion = "6.8.0"

    String jacocoVersion = "0.8.2"

    AutoconfExtension(Project project) {
        this.project = project
    }

    def defaultVersions(Map<String, String> versions) {
        versions.each { k, v -> defaultVersions.put(k, v) }
    }

    def junit5(boolean active) {
        junit5 = active
    }
}
