package com.mreil.gradleplugins.autoconf


import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction

class DefaultVersionsTask extends AbstractAutoconfTask {
    public static final String NAME = "defaultVersions"

    DefaultVersionsTask() {
        super()
        description = "Show default versions for dependencies"
    }

    @TaskAction
    def task() {
        AutoconfExtension extension =
                project.extensions.getByName(AutoconfExtension.NAME) as AutoconfExtension

        getProject().getLogger().log(LogLevel.WARN, "Default Versions: ")
        getProject().getLogger().log(
                LogLevel.WARN,
                extension.defaultVersions.collect { k, v -> "${k}: ${v}" }.join("\n")
        )
    }
}
