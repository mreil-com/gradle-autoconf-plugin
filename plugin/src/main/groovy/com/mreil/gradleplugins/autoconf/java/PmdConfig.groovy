package com.mreil.gradleplugins.autoconf.java

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import com.mreil.gradleplugins.autoconf.ConfigDownloader
import org.gradle.api.Project

/**
 * Configure PMD.
 * Executed in the afterEvaluate phase.
 */
class PmdConfig {
    static final def DEFAULT_PMD_CONFIG =
            "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/pmd/rules.xml"
    private String ruleSetFile

    def apply(Project project) {
        ruleSetFile = "${project.rootDir}/config/pmd/rules.xml"
        configurePlugin(project)
        configureDownloadTask(project)
    }

    def configurePlugin(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        project.with {
            apply plugin: "pmd"
            pmd {
                toolVersion = extension.pmdVersion
                ruleSetFiles = files(ruleSetFile)
                consoleOutput = false
            }

            [tasks.getByName("pmdMain"),
             tasks.getByName("pmdTest")].each {
                it.with {
                    reports {
                        xml.setEnabled false
                        html.setEnabled true
                    }
                }
            }
        }
    }

    def configureDownloadTask(Project project) {
        def ask = project.task("downloadPmdConfig").doLast {
            def dl = new ConfigDownloader(DEFAULT_PMD_CONFIG,
                    new File(ruleSetFile),
                    project.logger)
            dl.download()
        }

        project.tasks.getByName("pmdMain").dependsOn ask
        project.tasks.getByName("pmdTest").dependsOn ask
    }

}
