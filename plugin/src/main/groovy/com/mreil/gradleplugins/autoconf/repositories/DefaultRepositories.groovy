package com.mreil.gradleplugins.autoconf.repositories

import groovy.util.logging.Slf4j
import org.gradle.api.Project

@Slf4j
class DefaultRepositories {

    static apply(Project project) {
        project.allprojects.each { sub ->
            configureDefaultRepositories(sub)
        }
    }

    private static def configureDefaultRepositories(Project project) {
        log.info("Configuring default repositories for project: " + project)

        project.with {
            repositories {
                mavenLocal()
                jcenter()
                maven {
                    url "https://plugins.gradle.org/m2/"
                }
                mavenCentral()
            }
        }
    }
}
