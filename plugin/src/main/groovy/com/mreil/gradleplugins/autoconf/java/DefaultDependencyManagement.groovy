package com.mreil.gradleplugins.autoconf.java

import com.mreil.gradleplugins.autoconf.AutoconfExtension
import org.gradle.api.Project

import static com.mreil.gradleplugins.autoconf.ProjectHelper.rootPluginHasDependencyManagementPlugin

/**
 * Configure the dependency management plugin.
 * This is being executed in the afterEvaluate scope.
 */
class DefaultDependencyManagement {

    static apply(Project project) {
        if (rootPluginHasDependencyManagementPlugin(project)) {
            project.plugins.apply("io.spring.dependency-management")
            configureDefaultDependencyManagement(project)
        }
    }

    static def configureDefaultDependencyManagement(Project project) {
        def extension = project.rootProject.extensions
                .getByName(AutoconfExtension.NAME) as AutoconfExtension

        def defaultVersions = extension.defaultVersions

        project.dependencyManagement {
            dependencies {

                // TESTING

                dependency "junit:junit:${defaultVersions.junit4}"

                dependencySet(group: "org.junit.jupiter", version: defaultVersions.junit5) {
                    entry "junit-jupiter-api"
                    entry "junit-jupiter-engine"
                }
                dependency "org.junit.platform:junit-platform-launcher:${defaultVersions.junit5Platform}"

                dependency("org.spockframework:spock-core:${defaultVersions.spock}") {
                    exclude "org.codehaus.groovy:groovy-all"
                }
                dependency "org.objenesis:objenesis:${defaultVersions.objenesis}"

                dependency "com.github.tomakehurst:wiremock:${defaultVersions.wiremock}"

                // LOGGING

                dependencySet(group: "org.slf4j", version: defaultVersions.slf4j) {
                    entry "slf4j-api"
                    entry "slf4j-simple"
                    entry "jcl-over-slf4j"
                }

                dependencySet(group: "ch.qos.logback", version: defaultVersions.logback) {
                    entry "logback-classic"
                    entry "logback-core"
                }

                // DATABASE

                dependency group: "com.h2database", name: "h2", version: defaultVersions.h2

                // GROOVY

                dependency "org.codehaus.groovy:groovy:${defaultVersions.groovy}"


                // APACHE COMMONS
                dependency group: "org.apache.commons", name: "commons-collections4", version: defaultVersions.commonsCollections4
                dependency group: "org.apache.commons", name: "commons-lang3", version: defaultVersions.commonsLang3
                dependency group: "commons-io", name: "commons-io", version: defaultVersions.commonsIo
                dependency group: "commons-codec", name: "commons-codec", version: defaultVersions.commonsCodec


                // GOOGLE STUFF

                dependency group: "com.google.guava", name: "guava", version: defaultVersions.guava


                //


                dependencySet(group: "io.github.openfeign", version: defaultVersions.feign) {
                    entry "feign-core"
                    entry "feign-jackson"
                    entry "feign-slf4j"
                }

                dependency group: "org.eclipse.jgit", name: "org.eclipse.jgit", version: defaultVersions.jgit

                dependencySet(group: "com.fasterxml.jackson.core", version: defaultVersions.jackson) {
                    entry "jackson-core"
                    entry "jackson-databind"
                    entry "jackson-annotations"
                }
                dependencySet(group: "com.fasterxml.jackson.datatype", version: defaultVersions.jackson) {
                    entry "jackson-datatype-jdk8"
                    entry "jackson-datatype-jsr310"
                }
            }
        }
    }
}
