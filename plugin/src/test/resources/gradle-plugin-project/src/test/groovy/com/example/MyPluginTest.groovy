package com.example

import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class MyPluginTest extends Specification {
    @Rule
    private TemporaryFolder tmp = new TemporaryFolder()

    def "plugin test"() {
        when:
            def a = 1
        then:
            a == 1
    }
}
