package com.example

import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class MainGroovyTest extends Specification {
    @Rule
    private TemporaryFolder tmp = new TemporaryFolder()

    def "test"() {
        when: "bla"
            def a = 1
        then: "yo"
            a == 1
    }
}
