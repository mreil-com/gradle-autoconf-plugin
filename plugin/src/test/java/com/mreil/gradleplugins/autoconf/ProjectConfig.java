package com.mreil.gradleplugins.autoconf;

import lombok.Builder;
import lombok.NonNull;

import java.nio.file.Path;
import java.nio.file.Paths;

@Builder
public class ProjectConfig {

    @NonNull
    private String root;

    public Path getRootPath() {
        return Paths.get(root);
    }
}
