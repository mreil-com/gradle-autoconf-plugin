package com.mreil.gradleplugins.autoconf;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.rules.TemporaryFolder;

import java.nio.file.Path;
import java.util.Arrays;

@Slf4j
public class ProjectHelper {

    /**
     * Prepare a gradle project at a specific file system location.
     *
     * @param tmpRoot the temporary directory where the project should live
     * @param config the project configuration
     */
    @SneakyThrows
    public void setupProject(TemporaryFolder tmpRoot,
                             ProjectConfig config) {
        log.warn("Setting up project at: " + tmpRoot.getRoot());

        Path absRoot = config.getRootPath().toAbsolutePath();
        log.warn("Copying contents of: " + absRoot);

        FileUtils.copyDirectory(absRoot.toFile(), tmpRoot.getRoot());

        log.warn("Contents of project dir:");
        Arrays.stream(tmpRoot.getRoot().list()).forEach(
                log::warn
        );
    }
}
