package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

class AbstractGradleFuncTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    @Shared
    GradleRunner gradleRunner

    def setupSingleModuleProject() {
        setupGradleProject("single-module-project")
    }

    def setupSingleModuleProject(String gradleVersion) {
        setupGradleProject("single-module-project", gradleVersion)
    }

    def setupMultiModuleProject() {
        setupGradleProject("multi-module-project")
    }

    def setupGroovyProject() {
        setupGradleProject("groovy-project")
    }

    def setupGradleProject(String projectDir, String gradleVersion = null) throws IOException {
        def config = ProjectConfig.builder()
                .root("src/test/resources/" + projectDir)
                .build()
        new ProjectHelper().setupProject(testProjectDir, config)

        gradleRunner = GradleRunner.create()
                .withPluginClasspath()
                .withGradleVersion(gradleVersion ?: "5.1")
                .withProjectDir(testProjectDir.getRoot())
    }

    def setupGradlePluginProject() {
        setupGradleProject("gradle-plugin-project")
    }
}
