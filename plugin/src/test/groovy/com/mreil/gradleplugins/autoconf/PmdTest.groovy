package com.mreil.gradleplugins.autoconf

import groovy.util.logging.Slf4j
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

@Slf4j
class PmdTest extends AbstractGradleFuncTest {

    def "PMD report is generated"() {
        when: "I run the build"
            setupMultiModuleProject()
            def expectedFile = testProjectDir.root.toPath()
                    .resolve("java/build/reports/pmd/main.html")
                    .toFile()
            runTask("test", "pmdMain", "--info")

        then: "There is a coverage report"
            expectedFile.exists()

        cleanup: "Show report content"
            log.warn("Report content:")
            expectedFile.readLines().each {
                log.warn(it)
            }
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
