package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

class GradlePluginTest extends AbstractGradleFuncTest {

    def "Groovy build succeeds"() {
        def task = "assemble"
        when: "I run ${task}"
            setupGradlePluginProject()
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
