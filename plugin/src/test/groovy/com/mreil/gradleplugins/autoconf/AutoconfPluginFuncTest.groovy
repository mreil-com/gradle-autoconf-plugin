package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome
import org.gradle.testkit.runner.UnexpectedBuildFailure
import spock.lang.Ignore

class AutoconfPluginFuncTest extends AbstractGradleFuncTest {

    def "Applying plugin succeeds"() {
        setupSingleModuleProject()
        def task = "tasks"
        when:
            "I run ${task}"
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
    }

    // TODO make this non-func test
    @Ignore
    def "Plugin needs at least Gradle 4.6"() {
        setupSingleModuleProject("4.5")
        def task = "tasks"
        when:
            "I run ${task}"
            BuildResult result = runTask(task)
            System.err.println(result.output)

        then: "There is an error"
            UnexpectedBuildFailure ex = thrown()
            ex.message.contains "Plugin needs at least Gradle version 4.6"
    }

    def "defaultVersions task succeeds"() {
        setupSingleModuleProject()
        def task = "defaultVersions"
        when:
            "I run ${task}"
            BuildResult result = runTask(task)

        then: "There is no error"
            result.output.contains("commonsCollections4")
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
