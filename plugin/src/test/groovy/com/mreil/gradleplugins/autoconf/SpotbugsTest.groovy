package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

class SpotbugsTest extends AbstractGradleFuncTest {

    def "Spotbugs report is generated"() {
        when: "I run the build"
            setupMultiModuleProject()
            runTask("test", "spotbugsMain")

        then: "There is a coverage report"
            testProjectDir.root.toPath()
                    .resolve("java/build/reports/spotbugs/main.html")
                    .toFile().exists()
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
