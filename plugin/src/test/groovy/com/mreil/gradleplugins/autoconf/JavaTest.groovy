package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

class JavaTest extends AbstractGradleFuncTest {

    def "Java build succeeds"() {
        def task = "assemble"
        when: "I run ${task}"
            setupMultiModuleProject()
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
    }

    def "Jacoco report is generated"() {
        when: "I run the build"
            setupMultiModuleProject()
            runTask("test", "jacocoTestReport")

        then: "There is a coverage report"
            testProjectDir.root.toPath()
                    .resolve("java/build/reports/jacocoHtml/index.html")
                    .toFile().exists()
    }

    def "Publish to local maven succeeds"() {
        def task = "java:publishToMavenLocal"
        when: "I run ${task}"
            setupMultiModuleProject()
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
