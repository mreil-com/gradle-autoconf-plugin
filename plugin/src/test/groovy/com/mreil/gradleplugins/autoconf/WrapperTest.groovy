package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

class WrapperTest extends AbstractGradleFuncTest {

    def "Wrapper run succeeds"() {
        def task = "wrapper"
        when:
            "I run ${task}"
            setupMultiModuleProject()
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
            def wrapperProps = new File(testProjectDir.root,
                    "gradle/wrapper/gradle-wrapper.properties")
            wrapperProps.exists()
            wrapperProps.readLines()
                    .find { l -> l.matches("^distributionUrl=.*\$") }
                    .matches(".*-4\\.8-all.zip\$")
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
