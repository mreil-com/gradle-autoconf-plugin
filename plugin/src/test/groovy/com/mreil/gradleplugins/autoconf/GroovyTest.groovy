package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

class GroovyTest extends AbstractGradleFuncTest {

    def "Groovy build succeeds"() {
        def task = "assemble"
        when: "I run ${task}"
            setupGroovyProject()
            BuildResult result = runTask(task)
            System.err.println(result.output)

        then: "There is no error"
            hasNoError(result, task)
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
