package com.mreil.gradleplugins.autoconf

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.TaskOutcome

class DefaultRepositoriesTest extends AbstractGradleFuncTest {

    def "Test task exists in SMP"() {
        def task = "publish"
        when: "I run ${task}"
            setupSingleModuleProject()
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
    }

    def "Publish task exists in MMP"() {
        def task = "java:publish"
        when: "I run ${task}"
            setupMultiModuleProject()
            BuildResult result = runTask("${task}")

        then: "There is no error"
            hasNoError(result, task)
    }

    def "Publish task exists in SMP"() {
        def task = "test"
        when: "I run ${task}"
            setupSingleModuleProject()
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
    }

    def "Test task exists in MMP"() {
        def task = "java:test"
        when: "I run ${task}"
            setupMultiModuleProject()
            BuildResult result = runTask(task)

        then: "There is no error"
            hasNoError(result, task)
    }

    private static hasNoError(BuildResult result, String task) {
        [TaskOutcome.SUCCESS, TaskOutcome.UP_TO_DATE].contains(result.task(":${task}").outcome)
    }

    private runTask(String... tasks) {
        gradleRunner.withArguments(tasks)
                .build()
    }
}
