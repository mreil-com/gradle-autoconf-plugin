# gradle-autoconf-plugin

[![Maven version](https://img.shields.io/maven-metadata/v/https/plugins.gradle.org/m2/com/mreil/autoconf/com.mreil.autoconf.gradle.plugin/maven-metadata.xml.svg)](https://plugins.gradle.org/plugin/com.mreil.autoconf)
[![Build status](https://img.shields.io/bitbucket/pipelines/mreil-com/gradle-autoconf-plugin.svg)](https://bitbucket.org/mreil-com/gradle-autoconf-plugin)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> Automatically configure a Gradle project

## Install

Install the plugin as follows.

Only install the plugin in the root module of your project.

Use at least Gradle 5.6.

```
#!groovy

plugins {
    id "com.mreil.autoconf" version "0.0.5"
}

```


## Usage

### Configuration

The plugin can be configured as follows:

```
#!groovy

autoconf {
    gradleVersion = "4.9"      // set version for wrapper task
    lombok false               // disable Project Lombok 
    junit5 false               // disable JUnit 5 and use JUnit 4 instead
    checkstyleVersion = "8.12"
    spotbugsVersion = "3.1.3"
    codenarcVersion = "1.2.1"
    pmdVersion = "6.8.0"
    jacocoVersion = "0.8.2"
    defaultVersions[           // set versions for dependencies
        slf4j: "1.7.12",
        logback: "1.2.3"
    ]
}

```


### Tasks

#### defaultVersions

Shows all configured default versions for dependencies.


## Automatically applied configuration

### Repositories

For each project, the following repositories will be configured:

```
#!groovy

repositories {
    mavenLocal()
    jcenter()
    mavenCentral()
}
```


### Default Tasks

`test` and `publish` will be configured for each project.

Every project can be configured in your CI system wih the same build/test/publish commands 
even if the tasks are not defined by e.g. the `java` or `maven-publish` plugins.

The `wrapper` task will be configured.


### Java Projects

For every `java` project

* an HTML test report will be generated
* some default dependencies will be set
* the JUnit 5 framework will be configured
* [Checkstyle][checkstyle], [SpotBugs][spotbugs] amd [PMD][pmd] will be configured and reports will be generated during the `check`
phase
* JaCoCo coverage data will be collected while tests are executed and a report will be
generated
* the `maven-publish` plugin will be configured to support publishing to the local Maven
repository
* if applied, the `io.spring.dependency-management` plugin will be configured with default
versions for lots of different dependencies


### Groovy Projects

For every `groovy` project

* Codenarc will be configured and a report will be generated during the `check` phase
* Groovy and the Spock test framework will be configured


### Releasing Projects

The `net.researchgate.release` will be configured for every project, if applied.


## Changes

See [CHANGELOG.md][changelog] for changes.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


## Contribute

Please raise issues [here](../../issues?status=new&status=open).

Feel free to address existing issues and raise a [pull request](../../pull-requests).


## License

[MIT][license]


[changelog]: CHANGELOG.md
[license]: LICENSE
[checkstyle]: http://checkstyle.sourceforge.net/
[spotbugs]: https://spotbugs.github.io/
[pmd]: https://pmd.github.io/
